/* eslint-disable array-callback-return */
const { ApolloServer, gql } = require('apollo-server-express');
const mongoose = require('mongoose');
const express = require('express');

const bodyParser = require('body-parser');
const ramdomString = require('randomstring');
const axios = require('axios');

const app = express();
const port = 8080;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.connect(
  // 'mongodb+srv://hoangkhanh:01207996453@cluster0-uaq3u.mongodb.net/demo-mongo?retryWrites=true&w=majority',
  'mongodb://localhost/quanlydaily',
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);
const index_module = require('./models/index');
const Response = require('./response/Response');
const getLodash = require('lodash/get');
const get = require('lodash/get');
const set = require('lodash/set');
const { generate } = require('randomstring');
const typeDefs = gql`
  type Event {
    title: String!
    description: String!
    price: Float!
  }
  enum ADEDaiLyType {
    ADD
    DELETE
    EDIT
  }
  input SignInInput {
    user_name: String!
    password: String!
    remember_me: Boolean
  }

  input SignUpInput {
    user_name: String!
    password: String!
    name_user: String!
  }

  input inputDataProduct {
    nameProduct: String!
    page_size: String
    current_page: String
  }
  enum Profile_Type {
    GOOGLE
    FACEBOOK
    PROFILE_USER
  }
  input InputPofile {
    type: Profile_Type!
    token: String
    id: String
    user_name: String
    name_user: String
  }

  type ErrorsResponse {
    error_code: String!
    message: String!
  }
  type AccountResponse {
    status: String!
    message: String!
    errors: [ErrorsResponse]
    auth_token: String
  }
  type itemProduct {
    id: String!
    title: String
    price: String
    image: String
    imageSec: [String]
    amount: Int
    forphone: String
    type: String
    ranking: Int
    trademark: String
  }
  type itemDaily {
    id_daily: String
    name: String
    type: String
    district: String
    manager: String
    address: String
    phone: String
  }

  type itemHoaDon {
    id_daily:  String!
    id_hoadon: String!
    id_phieuthu: String!
    name: String!
    date: String!
    sum: SumPrice!
    list_product: [ItemProductHD]!
  }
  type SumPrice {
    amount: Int!
    total_price: Int!
  }
  type ItemProductHD {
    dongia:  String!
    dvtinh:  String!
    name: String!
    soluong:  String!
    stt:  String!
    total_price:  String!
  }

  type ListItemId {
    status: String!
    message: String
    errors: [ErrorsResponse]
    total_item: Int
    list_item: [itemProduct]
  }
  type ListDaily {
    status: String!
    message: String
    errors: [ErrorsResponse]
    list_daily: [itemDaily]
  }
  type ListHoaDon {
    status: String!
    message: String
    errors: [ErrorsResponse]
    list_hoadon: [itemHoaDon]
  }
  type ListKhuyenMai {
    status: String!
    message: String
    errors: [ErrorsResponse]
    list_khuyenmai: [itemKhuyenMai]

    
  }
  type ListPhieuThuTien {
    status: String!
    message: String
    errors: [ErrorsResponse]
    list_phieuthutien: [itemPhieuThuTien]

    
  }
  type ListPhieuXH {
    status: String!
    message: String
    errors: [ErrorsResponse]
    list_phieu_xh: [itemPhieuXH]
  }
  type ListID {
    list_id: itemID
  }
  type ItemPXH {
    status: String!
    message: String
    errors: [ErrorsResponse]
    list_hoadon: [itemHoaDon]
    list_phieu_xh: [itemPhieuXH]
  }
  type itemID {
    id_daily: [String]
    id_hoadon:[String]
    maKM: [itemKhuyenMai]
    id_phieuthu:[String]
    id_phieuxuat:[String]
  }
  type itemPhieuXH {
    id_phieuxuat: String!
    id_daily: String!
    id_hoadon: String!
    id_phieuthu: String!
    ngaylapphieu: String!
    maKM: String!
    tienthu: String!
    conno: String!
  }
  type itemPhieuThuTien {
    id_daily: String!
    id_hoadon: String!
    id_phieuthu: String!
    ngaythutien: String!
    tienthu: String!
    conno: String!
    maKM: String!

  }
  type itemKhuyenMai {
    maKM: String!
    thoihan : String!
    tilechietkhau : String!
    ngaylapphieu : String!
  }
  type resOrderCart {
    status: String!
    message: String
    errors: [ErrorsResponse]
  }
  input DaiLyInfor {
    id_daily: String
    name: String
    type: String
    manager: String
    district: String
    address: String
    phone: String
  }
  input PhieuXHInfor {
    id_phieuxuat: String
    id_daily: String!
    id_hoadon: String!
    id_phieuthu: String!
    ngaylapphieu: String!
    maKM: String
    tienthu: String!
    conno: String!
  }
  input HoaDonInfor {
    list_product: [arrSP]
    date: String
    id_hoadon: String
    id_phieuthu: String
    id_daily: String
    name:  String
    sum: String
  }
  input PTTInfor {
    id_daily:  String
    id_hoadon:  String
    id_phieuthu:  String
    ngaythutien:  String
    tienthu:  String
    maKM:  String
  }
  input KMInfor {
    maKM: String
    thoihan: String
    tilechietkhau: String
    ngaylapphieu: String
  }

  input idItem {
    id_item: [String!]
  }
  input cartUser {
    token: String
    itemOrder: inforItem!
    NgayDatHang: String
  }
  input InputDaiLy {
    valueInputDaily: DaiLyInfor
    type: ADEDaiLyType
  }
  input InputPhieuXH {
    valueInputPhieuXH: PhieuXHInfor
    type: ADEDaiLyType
  }

  input arrSP {
    dongia:  String
    dvtinh:  String
    name: String
    soluong:  String
    stt:  String
    total_price:  String

  }
  input InputHoaDon {
    valueInputHoaDon: HoaDonInfor
    type: ADEDaiLyType
  }
  input InputKM {
    valueInputKM: KMInfor
    type: ADEDaiLyType
  }
  input InputPTT {
    valueInputPTT: PTTInfor
    type: ADEDaiLyType
  }
  input inforItem {
    id_item: [String!]
    amount: [Int]
  }
  input inputIDHOADON {
    id_hoadon: String
  }
  type students {
    name_user: String!
    token: String!
  }
  type dataInforUser {
    status: String!
    message: String!
    errors: [ErrorsResponse]
    student: students
  }
  type list_item_Search {
    status: String!
    message: String!
    errors: [ErrorsResponse]
    total_item: Int
    list_item: [itemProduct]
  }
  type resADEDaiLy {
    status: String!
    message: String!
    errors: [ErrorsResponse]
  }
  type resADEHoaDon {
    status: String!
    message: String!
    errors: [ErrorsResponse]
  }
  type resDataProduct {
    status: String!
    message: String
    errors: [ErrorsResponse]
    total_item: Int
    list_item: [itemProduct]!
  }
  type Query {
    events: [Event!]!
    getDataProduct(input: inputDataProduct!): resDataProduct!
    getItemId(input: idItem!): ListItemId!
    getListDaily: ListDaily!
    getListHoaDon(input: String): ListHoaDon!
    getListKhuyenMai(input: String): ListKhuyenMai!
    getListPhieuThuTien: ListPhieuThuTien!
    getListPhieuXH: ListPhieuXH!
    getID: ListID
    getItemPhieuXuatHang(input: String): ItemPXH!
  }
  type Mutation {
    signIn(input: SignInInput!): AccountResponse!
    signUp(input: SignUpInput): AccountResponse!
    getInforUser(input: InputPofile!): dataInforUser
    orderCart(input: cartUser!): resOrderCart!
    searchItem(input: String!): list_item_Search!
    ADEDaiLy(input: InputDaiLy!): resADEDaiLy!
    ADEHoaDon(input: InputHoaDon): resADEHoaDon!
    ADEKM(input: InputKM): resADEDaiLy!
    ADEPTT(input: InputPTT): resADEDaiLy!
    ADEPhieuXH(input: InputPhieuXH!): resADEDaiLy!
  }
`;
const deduplicate = (arr) => {
  let isExist = (arr, x) => {
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].id === x.id) return true;
    }
    return false;
  };
  let ans = [];
  arr.forEach((element) => {
    if (!isExist(ans, element)) ans.push(element);
  });
  return ans;
};
const getItemId_func = async (id_item) => {
  const value = await index_module.Model_Product.find().then((res) => {
    let arrItem = [];
    res.find((item) => {
      for (let index in item.ItemProduct) {
        id_item.map((res_id_item) => {
          if (item.ItemProduct[index].id === res_id_item) {
            arrItem.push(item.ItemProduct[index]);
          }
        });
      }
    });
    return deduplicate(arrItem);
  });
  return value;
};
const getItemName_func = async (valueSearch) => {
  const value = await index_module.Model_Product.find().then((res) => {
    let arrItem = [];
    res.find((item) => {
      for (let index in item.ItemProduct) {
        let search_item = new RegExp(`\\b${valueSearch.toUpperCase()}`).exec(
          item.ItemProduct[index].title.toUpperCase()
        );
        let search_type = new RegExp(`\\b${valueSearch.toUpperCase()}`).exec(
          item.ItemProduct[index].type.toUpperCase()
        );
        if (search_item !== null || search_type !== null) {
          arrItem.push(item.ItemProduct[index]);
        }
      }
    });
    return deduplicate(arrItem);
  });
  return value;
};
const getItemDaily = async () => {
  const value = await index_module.Model_ListDaily.find().then((res) => res);
  return value;
};
const ADD_DaiLy = (data) => {
  const { _Id, _Name, _Type, _Manager, _District, _Address, _Phone } = data;
  if ( !_Name || !_Type || !_Manager || !_District || !_Address || !_Phone ) return;
  const addDaily = new index_module.Model_ListDaily({
    id_daily: ramdomString.generate(6),
    name: _Name,
    type: _Type,
    manager: _Manager,
    district: _District,
    address: _Address,
    phone: _Phone,
  });

  addDaily.save((err) => {
    if (err) console.log('err: ', err);
  });
  return Response.AddDaiLySuccess();
};

const DELETE_DaiLy = (data) => {
  const { _Id } = data;
  if ( !_Id ) return;
  const addDaily = new index_module.Model_ListDaily();

  index_module.Model_ListDaily.deleteOne(
    { id_daily: _Id},
    ).then(() => {});
  return Response.DeleteDaiLySuccess();
}
const EDIT_DaiLy =async (data) => {
  const { _Id, _Name, _Type, _Manager, _District, _Address, _Phone } = data;
  if ( !_Id ) return;
  const value = await index_module.Model_ListDaily.find({id_daily: _Id}, (err , res) => {
    const name = !_Name ? get(res[0],'name',_Name) : _Name;
    const type = !_Type ? get(res[0],'type',_Type) : _Type;
    const district = !_District ? get(res[0],'district',_District) : _District;
    const address = !_Address ? get(res[0],'address',_Address) : _Address;
    const manager = !_Manager ? get(res[0],'manager',_Manager) : _Manager;
    const phone = !_Phone ? get(res[0],'phone',_Phone): _Phone;

    index_module.Model_ListDaily.updateOne(
      { id_daily: _Id},
      {$set: {
      "name" :name,
      "type" : type,
      "district" : district,
      "manager" : manager,
      "address" : address,
      "phone" : phone,
      }}
      ).then(() => {});
  })
  return Response.DeleteDaiLySuccess();
}
const ADD_PhieuXH = async (data) => {
  const { _Id_PhieuXH, _Id_DaiLy, _Id_Hoadon, _NgayLapPhieu, _MaKM, _TienThu, _ConNo, _Id_PhieuThu } = data;

  
  const addDaily = new index_module.Model_Phieu_XH({
    id_phieuxuat: ramdomString.generate(6),
    id_daily: _Id_DaiLy,
    id_hoadon: _Id_Hoadon,
    id_phieuthu: _Id_PhieuThu,
    ngaylapphieu: _NgayLapPhieu,
    maKM: _MaKM,
    tienthu: _TienThu,
    conno: _ConNo,
  });

  await addDaily.save((err) => {
    if (err) console.log('err: ', err);
    console.log("don")
  });
  return Response.AddDaiLySuccess();
};
const DELETE_PhieuXH=async (data) => {
  const { _Id_PhieuXH } = data;
  await index_module.Model_Phieu_XH.deleteOne(
    { id_phieuxuat: _Id_PhieuXH},
    ).then((res) => {}).catch((err) => console.log("DELETE_HoaDon",err));
  return Response.DeleteDaiLySuccess();
}
const EDIT_PhieuXH  = async (data) => {
  const { _Id_PhieuXH, _Id_DaiLy, _Id_Hoadon, _NgayLapPhieu, _MaKM, _TienThu, _ConNo, _Id_PhieuThu } = data;
    await  index_module.Model_Phieu_XH.updateOne(
      { id_phieuxuat: _Id_PhieuXH},
      {$set: {
        id_daily: _Id_DaiLy,
        id_hoadon: _Id_Hoadon,
        id_phieuthu: _Id_PhieuThu,
        ngaylapphieu: _NgayLapPhieu,
        maKM: _MaKM,
        tienthu: _TienThu,
        conno: _ConNo,
      }}
      ).then(() => {console.log("done");});
  return Response.AddDaiLySuccess();
};
const ADD_HoaDon = (data) => {
  const { _Id, _Name, _Date, _List_product, _Sum } = data;

  let sumSP = _Sum;
  let sumSL = 0;
  _List_product.map(item => {
    if(!item) return;
    const soluong = parseInt(item.soluong,10);
    sumSL +=soluong;

  });

  const addHoaDon = new index_module.Model_Hoa_Don({
    id_daily: _Id,
    id_phieuthu: ramdomString.generate(6),
    id_hoadon: ramdomString.generate(6),
    date: _Date,
    name: _Name,
    sum: {
      amount: sumSL,
      total_price: sumSP
    },
    list_product: _List_product
  });

  addHoaDon.save((err) => {
    if (err) console.log('err: ', err);
  });
  return Response.AddDaiLySuccess();
};

const DELETE_HoaDon =async (data) => {
  const { _Id, _Id_Hoadon } = data;
  const dataHoaDon =await new index_module.Model_Hoa_Don()
  index_module.Model_Hoa_Don.deleteOne(
    { id_hoadon: _Id_Hoadon},
    ).then((res) => {}).catch((err) => console.log("DELETE_HoaDon",err));
  return Response.DeleteDaiLySuccess();
}
const EDIT_HoaDon =async (data) => {
  const { _Id, _Id_Hoadon, _Id_PhieuThu, _Name, _Date, _List_product } = data;
  
  let sumSP = 0;
  let sumSL = 0;
  _List_product.map(item => {
    sumSP += parseInt(item.dongia,10)*parseInt(item.soluong,10)
    sumSL +=parseInt(item.soluong,10);
  });
  const dataHoaDon =await new index_module.Model_Hoa_Don()
     index_module.Model_Hoa_Don.updateOne(
      { id_hoadon: _Id_Hoadon},
      {$set: {
        id_daily : _Id,
        id_phieuthu :_Id_PhieuThu,
        id_hoadon : _Id_Hoadon,
        date : _Date,
        name : _Name,
        sum: {
          amount: sumSL,
          total_price: sumSP
        },
        list_product: _List_product
      }}
      ).then(() => {});
  return Response.AddDaiLySuccess();
}
const ADD_KM =async (data) => {
  const { _MaKM, _ThoiHan, _TiLeChietKhau, _NgayLapPhieu } = data;

  const addKM =await new index_module.Model_Khuyen_Mai({
    maKM: ramdomString.generate(15),
    thoihan: _ThoiHan,
    tilechietkhau: _TiLeChietKhau,
    ngaylapphieu: _NgayLapPhieu,
  });
  
  addKM.save((err) => {
    if (err) console.log('err: ', err);
  });
  return Response.AddDaiLySuccess();
};
const EDIT_KM = async (data) => {
  const { _MaKM, _ThoiHan, _TiLeChietKhau, _NgayLapPhieu } = data;

    await index_module.Model_Khuyen_Mai.updateOne(
      { maKM: _MaKM},
      {$set: {
        maKM: _MaKM,
        thoihan: _ThoiHan,
        tilechietkhau: _TiLeChietKhau,
        ngaylapphieu: _NgayLapPhieu,
      }}
      ).then(() => {console.log("done");});
      console.log("co");
    
  return Response.AddDaiLySuccess();
};
const DELETE_KM = async (data) => {
  const { _MaKM, _ThoiHan, _TiLeChietKhau, _NgayLapPhieu } = data;

    await index_module.Model_Khuyen_Mai.deleteOne(
      { maKM: _MaKM},
      ).then(() => {console.log("done");});
      console.log("co");
    
  return Response.AddDaiLySuccess();
}
const ADD_PTT = async (data) => {
  const {_Id_DaiLy, _Id_Hoadon, _Id_PhieuThu, _NgayThuTien, _TienThu, _MaKM } = data

  const value = await index_module.Model_Hoa_Don.find().then((res) => res.filter(item => item.id_hoadon === _Id_Hoadon && item));
  const valueMaKM = await index_module.Model_Khuyen_Mai.find().then((res) => res.filter(item => item.maKM === _MaKM && item));
  const valuePTT = await index_module.Model_Phieu_Thu_Tien.find().then((res) => res.filter(item => item.id_hoadon === _Id_Hoadon && item));
  

  const soTienTra = parseInt(_TienThu);
  const total_money = parseInt(value[0].sum.total_price);
  const moneyRemain = parseInt(get(valuePTT[0],"conno", 0));

  let soTienKhuyemai  = 0;
  if(valueMaKM.length > 0){
    soTienKhuyemai=((total_money*parseInt(valueMaKM[0].tilechietkhau,10)) / 100)
  }

  let money = total_money ;
  if(moneyRemain > 0)
    money = moneyRemain
  const connoNew =money - (soTienTra + soTienKhuyemai);


  const addPTT =await new index_module.Model_Phieu_Thu_Tien({
    id_daily: _Id_DaiLy,
    id_hoadon: _Id_Hoadon,
    id_phieuthu:ramdomString.generate(6),
    ngaythutien: _NgayThuTien,
    tienthu: _TienThu,
    conno: connoNew.toString(),
    maKM: _MaKM
  });
  
  addPTT.save((err) => {
    if (err) console.log('err: ', err);
  });
  return Response.AddDaiLySuccess();

}
const EDIT_PTT = async (data) => {
  const {_Id_DaiLy, _Id_Hoadon, _Id_PhieuThu, _NgayThuTien, _TienThu, _MaKM } = data
  const value = await index_module.Model_Hoa_Don.find().then((res) => res.filter(item => item.id_hoadon === _Id_Hoadon && item));
  const valueMaKM = await index_module.Model_Khuyen_Mai.find().then((res) => res.filter(item => item.maKM === _MaKM && item));
  

  const soTienTra = parseInt(_TienThu);
  const total_money = parseInt(value[0].sum.total_price);
  let soTienKhuyemai  = 0;
  if(valueMaKM.length > 0){
    soTienKhuyemai=((total_money*parseInt(valueMaKM[0].tilechietkhau,10)) / 100)
  }

  
  const connoNew =total_money - (soTienTra + soTienKhuyemai);

    await index_module.Model_Phieu_Thu_Tien.updateOne(
      { id_phieuthu: _Id_PhieuThu},
      {$set: {
        id_daily: _Id_DaiLy,
        id_hoadon: _Id_Hoadon,
        id_phieuthu: _Id_PhieuThu,
        ngaythutien: _NgayThuTien,
        tienthu: _TienThu,
        conno: connoNew.toString(),
        maKM: _MaKM
      }}
      ).then(() => {console.log("done");});
  return Response.AddDaiLySuccess();
};
const DELETE_PTT = async (data) => {
  const {_Id_DaiLy, _Id_Hoadon, _Id_PhieuThu, _NgayThuTien, _TienThu } = data
    await index_module.Model_Phieu_Thu_Tien.deleteOne(
      { id_phieuthu: _Id_PhieuThu},
      ).then(() => {console.log("done");});
  return Response.AddDaiLySuccess();
}
const resolvers = {
  Query: {
    getDataProduct: async (root, args) => {
      const { nameProduct } = args.input;
      const value = await index_module.Model_Product.find()
        .then((res) =>
          res.find((item) => {
            if (item.nameProduct === nameProduct) {
              return item.ItemProduct;
            }
          })
        )
        .catch((err) => {
          console.log('err: ', err);
        });
      return Response.dataProductItem_Success(value.ItemProduct);
    },
    events: async () => {
      index_module.Model_Event.find()
        .then((res) => res.map((item) => ({ ...item._doc })))
        .catch((err) => {
          console.log('err: ', err);
        });
    },
    getItemId: async (root, args) => {
      const { id_item } = args.input;
      let value;
      if (id_item !== undefined) {
        value = await getItemId_func(id_item);
      }
      return Response.dataProductItem_Success(value);
    },
    getListDaily: async (root, args) => {
      const value = await getItemDaily();
      return Response.dataList_DaiLy_Success(value.reverse());
    },
    getListHoaDon: async (root, args) => {
      let value = null;
      const id_hoadon = get(args,'input.id_hoadon','') ;
      if(!id_hoadon)
       value = await  index_module.Model_Hoa_Don.find().then(res => res)
      else 
      value = await  index_module.Model_Hoa_Don.findOne({id_hoadon: id_hoadon}).then(() =>{})

      return Response.dataList_HoaDon_Success(value);
    },
    getListKhuyenMai:  async (root, args) => {
      let value = null;
      const maKM = !get(args,'input.maKM','') ;
      if(!get(args,'input.maKM',''))
       value = await index_module.Model_Khuyen_Mai.find().then(res => res)
      else 
      value = await index_module.Model_Khuyen_Mai.findOne({maKM: maKM}).then(() =>{})
      return Response.dataList_KhuyenMai_Success(value.reverse());
    },
    getListPhieuThuTien: async (root, args) => {
      let data = [];
      const value = await index_module.Model_Phieu_Thu_Tien.find().then((res) =>{data = [...res]})
      return Response.dataList_ThuTien_Success(data.reverse());
    },
    getListPhieuXH : async (root, args) => {
      let data = [];
      const value = await index_module.Model_Phieu_XH.find().then((res) =>{data = [...res]})
      console.log(data);
      return Response.dataList_XH_Success(data.reverse());
    },
    getID: async (root, args) => {
      let dataID = {};
      let stt = 0;
      await index_module.Model_ListDaily.find().then((res) =>{
        res.map((item,index) => set(dataID,["id_daily", index], item.id_daily ))
      });
      await index_module.Model_Hoa_Don.find().then((res) =>{
        res.map((item,index) => set(dataID,["id_hoadon", index], item.id_hoadon ))
      });
      await index_module.Model_Khuyen_Mai.find().then((res) =>{
        res.map((item,index) => set(dataID,["maKM", index], item ))

      });

      await index_module.Model_Phieu_Thu_Tien.find().then((res) =>{
        res.map((item,index) => set(dataID,["id_phieuthu", index], item.id_phieuthu ))
      });
      await index_module.Model_Phieu_XH.find().then((res) =>{
        res.map((item,index) => set(dataID,["id_phieuxuat", index], item.id_phieuxuat ))
      });
      return Response.dataList_ID_Success(dataID);
    },
    getItemPhieuXuatHang : async (root, args) => {
      const _id_phieuxuat = args.input;

      const dataPXH = await  index_module.Model_Phieu_XH.findOne({id_phieuxuat: _id_phieuxuat}).then((res) => res);
      const dataHD = await  index_module.Model_Hoa_Don.findOne({id_hoadon: dataPXH.id_hoadon}).then((res) =>res)
      
      const data =[dataPXH,dataHD]
      console.log(data);

      return Response.dataList_XH_ChiTiet_Success(dataPXH,dataHD);
    },
  },
  Mutation: {
    // du lieu la args
    signIn: async (root, args) => {
      const SignInInput = args.input;
      const value = await index_module.Model_Account.find()
        .then((res) => {
          const account = res.find((dataUser) => {
            return dataUser.user_name === SignInInput.user_name;
          });
          if (!account) {
            return Response.ErrorEmail();
          }
          if (SignInInput.password === account.password) {
            const tokenRandomString = ramdomString.generate(105);
            // update token
            /*--------------------------------------------------*/
            index_module.Model_ProfileUser.find()
              .then(async (resfindToek) => {
                await resfindToek.filter((dataProfile) => {
                  if (dataProfile.token === account.token) {
                    index_module.Model_Account.updateOne(
                      { token: account.token },
                      { $set: { token: tokenRandomString } }
                    ).then(() => {});
                    index_module.Model_ProfileUser.updateOne(
                      { token: account.token },
                      { $set: { token: tokenRandomString } }
                    ).then(() => {});
                  }
                });
              })
              .catch((err) => console.log('err: ', err));
            /*--------------------------------------------------*/
            return Response.SignInUpSuccess(tokenRandomString);
          }
          return Response.ErrorPassowrd();
        })
        .catch((err) => {
          console.log('err: ', err);
        });
      return value;
    },
    getInforUser: async (root, args) => {
      const inputProfile = args.input;
      const value = await index_module.Model_ProfileUser.find().then((res) =>
        res.find((resdataProfile) => {
          return (
            resdataProfile.token === inputProfile.token ||
            (resdataProfile.user_name === inputProfile.user_name &&
              resdataProfile.type === inputProfile.type)
          );
        })
      );
      if (value) {
        return Response.ExistProfileUser(value);
      } else if (
        inputProfile.type === 'GOOGLE' ||
        (inputProfile.type === 'FACEBOOK' &&
          inputProfile.user_name !== undefined &&
          inputProfile.user_name !== '' &&
          inputProfile.name_user !== undefined &&
          inputProfile.name_user !== '')
      ) {
        const idCartsRandomString = ramdomString.generate(35);
        const tokenRandomString = ramdomString.generate(105);
        const createProfileUser = new index_module.Model_ProfileUser({
          type: inputProfile.type,
          id: inputProfile.id,
          token: tokenRandomString,
          name_user: inputProfile.name_user,
          user_name: inputProfile.user_name,
          carts: idCartsRandomString,
        });
        createProfileUser.save((err) => {
          if (err) console.log('err: ', err);
        });
        return Response.ExistProfileUser(inputProfile);
      }
      return Response.ErrorNotExistProfileUser();
    },
    signUp: async (root, args) => {
      const SignUpInput = args.input;
      const value = await index_module.Model_Account.find().then((res) => {
        const account = res.find((dataUser) => {
          return dataUser.user_name === SignUpInput.user_name;
        });
        if (!account) {
          const idCartsRandomString = ramdomString.generate(35);
          const tokenRandomString = ramdomString.generate(105);
          const createAccount = new index_module.Model_Account({
            token: tokenRandomString,
            user_name: SignUpInput.user_name,
            password: SignUpInput.password,
          });
          createAccount.save((err) => {
            if (err) console.log('err: ', err);
          });
          const createProfileUser = new index_module.Model_ProfileUser({
            type: 'PROFILE_USER',
            token: tokenRandomString,
            user_name: SignUpInput.user_name,
            name_user: SignUpInput.name_user,
            carts: idCartsRandomString,
          });
          createProfileUser.save((err) => {
            if (err) console.log('err: ', err);
          });
          return Response.SignInUpSuccess(tokenRandomString);
        }
        return Response.ErrorEmailExist();
      });
      return value;
    },
    orderCart: async (root, args) => {
      const { token, itemOrder, NgayDatHang } = args.input;
      const checklistOrderItem = await getItemId_func(itemOrder.id_item);
      let listItemOrder = [];
      for (let index in checklistOrderItem) {
        itemOrder.id_item.map((item) => {
          if (checklistOrderItem[index].id === item) {
            listItemOrder.push({
              id_item: checklistOrderItem[index].id,
              amount: itemOrder.amount[index],
            });
          }
        });
      }
      const id_carts = await index_module.Model_ProfileUser.find().then(
        (res) => {
          const user = res.find((item) => {
            return item.token === token;
          });
          return user.carts;
        }
      );
      const findCarts = await index_module.Model_Carts.find().then((res) =>
        res.find((item) => {
          return item.id_carts === id_carts;
        })
      );
      const orderItem = [];
      listItemOrder.map((item) => {
        orderItem.push({
          item: item.id_item,
          amount: item.amount,
        });
      });
      if (!findCarts) {
        const insertCart = new index_module.Model_Carts({
          id_carts: id_carts,
          listOrder: [
            {
              date: NgayDatHang,
              orderItem: orderItem,
            },
          ],
        });
        insertCart.save((err) => {
          if (err) console.log('err: ', err);
          console.log('order sucessful');
        });
      } else {
        index_module.Model_Carts.findOneAndUpdate(
          { id_carts: id_carts },
          {
            $push: {
              listOrder: {
                date: NgayDatHang,
                orderItem: orderItem,
              },
            },
          }
        ).then(() => {});
      }
    },
    searchItem: async (root, args) => {
      const valueSeach = args.input;
      const value = await getItemName_func(valueSeach);
      if (value.length === 0) {
        return Response.dataProductItem_Errors();
      }
      return Response.dataProductItem_Success(value);
    },
    ADEDaiLy: async (root, args) => {
      const { input } = args;
      const _TypeDaiLy = getLodash(input, 'type', '');
      const InputDaiLy = {
        _Id: getLodash(input, 'valueInputDaily.id_daily', ''),
        _Name: getLodash(input, 'valueInputDaily.name', ''),
        _Type: getLodash(input, 'valueInputDaily.type', ''),
        _Phone: getLodash(input, 'valueInputDaily.phone', ''),
        _Manager: getLodash(input, 'valueInputDaily.manager', ''),
        _Address: getLodash(input, 'valueInputDaily.address', ''),
        _District: getLodash(input, 'valueInputDaily.district', ''),
      };
      switch (_TypeDaiLy) {
        case 'ADD':
          ADD_DaiLy(InputDaiLy);
          break;
        case 'DELETE':
          DELETE_DaiLy(InputDaiLy);
          break;
        case 'EDIT':
          EDIT_DaiLy(InputDaiLy);
          break;
      }
    },
    ADEPhieuXH: async (root, args) => {
      const { input } = args;
      const _TypeDaiLy = getLodash(input, 'type', '');
      const valuePhieuXH = getLodash(input, 'valueInputPhieuXH', {})
      console.log(valuePhieuXH);
      const InputDaiLy = {
        _Id_PhieuXH: getLodash(valuePhieuXH,'id_phieuxuat', ''),
        _Id_DaiLy: getLodash(valuePhieuXH,'id_daily', ''),
        _Id_Hoadon: getLodash(valuePhieuXH,'id_hoadon', ''),
        _Id_PhieuThu: getLodash(valuePhieuXH,'id_phieuthu', ''),
        _NgayLapPhieu: getLodash(valuePhieuXH,'ngaylapphieu', ''),
        _MaKM: getLodash(valuePhieuXH,'maKM', ''),
        _TienThu: getLodash(valuePhieuXH,'tienthu',0),
        _ConNo: getLodash(valuePhieuXH,'conno', ''),
      };
      switch (_TypeDaiLy) {
        case 'ADD':
          return ADD_PhieuXH(InputDaiLy);
        case 'DELETE':
          return DELETE_PhieuXH(InputDaiLy);
        case 'EDIT':
          return EDIT_PhieuXH(InputDaiLy);
      }
    },
    ADEHoaDon: async (root, args) => {
      const { input } = args;
      const _TypeDaiLy = getLodash(input, 'type', '');
      const InputDaiLy = {
        _Id: getLodash(input, 'valueInputHoaDon.id_daily', ''),
        _Id_Hoadon: getLodash(input, 'valueInputHoaDon.id_hoadon', ''),
        _Id_PhieuThu: getLodash(input, 'valueInputHoaDon.id_phieuthu', ''),
        _Name: getLodash(input, 'valueInputHoaDon.name', ''),
        _Date: getLodash(input, 'valueInputHoaDon.date', ''),
        _List_product: getLodash(input, 'valueInputHoaDon.list_product', ''),
        _Sum: getLodash(input, 'valueInputHoaDon.sum', ''),
      };
      console.log(input);
            switch (_TypeDaiLy) {
        case 'ADD':
          ADD_HoaDon(InputDaiLy);
          break;
        case 'EDIT':
            EDIT_HoaDon(InputDaiLy);
            break;
        case 'DELETE':
          DELETE_HoaDon(InputDaiLy);
          break;
      }
    },
    ADEKM: async (root, args) => {
      const { input } = args;
      const _TypeDaiLy = getLodash(input, 'type', '');
      const valueInput  =  getLodash(input,"valueInputKM", {});
      const InputDaiLy = {
        _MaKM: getLodash(valueInput,'maKM', ''),
        _ThoiHan: getLodash(valueInput,'thoihan', ''),
        _TiLeChietKhau: getLodash(valueInput,'tilechietkhau', ''),
        _NgayLapPhieu: getLodash(valueInput,'ngaylapphieu', ''),
      };
      switch (_TypeDaiLy) {
        case 'ADD':
          return ADD_KM(InputDaiLy);
        case 'EDIT':
          return EDIT_KM(InputDaiLy);
        case 'DELETE':
          return DELETE_KM(InputDaiLy);
      }
    },
    ADEPTT: async (root, args) => {
      const { input } = args;
      const _TypeDaiLy = getLodash(input, 'type', '');
      const valueInput  =  getLodash(input,"valueInputPTT", {});
      const InputDaiLy = {
        _Id_DaiLy: getLodash(valueInput,'id_daily', ''),
        _Id_Hoadon: getLodash(valueInput,'id_hoadon', ''),
        _Id_PhieuThu: getLodash(valueInput,'id_phieuthu', ''),
        _NgayThuTien: getLodash(valueInput,'ngaythutien', ''),
        _TienThu: getLodash(valueInput,'tienthu', ''),
        _MaKM: getLodash(valueInput,'maKM', ''),

      };
      switch (_TypeDaiLy) {
        case 'ADD':
          return ADD_PTT(InputDaiLy);
        case 'EDIT':
          return EDIT_PTT(InputDaiLy);
        case 'DELETE':
          return DELETE_PTT(InputDaiLy);
      }
    },
  },
};
const server = new ApolloServer({
  typeDefs,
  resolvers,
});
// const fs = require('fs');
// let dataJson = fs.readFileSync('data.json');
// const { list_hoadon } = JSON.parse(dataJson);
// let count = 0;
//   for(let index in list_hoadon){
//     console.log(list_hoadon[index].id_daily)
//     const insert = index_module.Model_Hoa_Don({
//       id_daily: list_hoadon[index].id_daily,
//       id_hoadon: list_hoadon[index].id_hoadon,
//       id_phieuthu: list_hoadon[index].id_phieuthu,
//       name: list_hoadon[index].name,
        
//       date: list_hoadon[index].date,
//       type: list_hoadon[index].type,
//       list_product: list_hoadon[index].list_product,
//       sum: list_hoadon[index].sum,
//     });
//     insert.save((err) => {
//       if (err) console.log(err);
//       console.log('done.' +  count);
//     });
//     count++;
// }
server.applyMiddleware({ app, path: '/graphql' });
app.listen({ port }, () => {
  console.log('Apollo Server on http://localhost:8080/graphql');
});
