module.exports.SignInUpSuccess = (token) => {
  return {
    status: 'OK',
    message: 'SignIn Success',
    errors: null,
    auth_token: token,
  };
};
module.exports.AddDaiLySuccess = (token) => {
  return {
    status: 'OK',
    message: 'Create Success',
    errors: null,
  };
};
module.exports.DeleteDaiLySuccess = (token) => {
  return {
    status: 'OK',
    message: 'Delete Success',
    errors: null,
  };
};
module.exports.ErrorPassowrd = () => {
  return {
    status: 'ERROR',
    message: 'Có lỗi xảy ra.',
    errors: [
      {
        error_code: 'ERR_USR_002',
        message: 'Mật khẩu không đúng.',
      },
    ],
    auth_token: null,
  };
};
module.exports.ErrorEmail = () => {
  return {
    status: 'ERROR',
    message: 'Có lỗi xảy ra.',
    errors: [
      {
        error_code: 'ERR_USR_001',
        message: 'Không tìm thấy người dùng.',
      },
    ],
    auth_token: null,
  };
};
module.exports.ErrorEmailExist = () => {
  return {
    status: 'ERROR',
    message: 'Có lỗi xảy ra.',
    errors: [
      {
        error_code: 'ERR_USR_003',
        message: 'Tài khoản đã tồn tại',
      },
    ],
  };
};
module.exports.ExistProfileUser = (students) => {
  console.log(students.name_user);
  return {
    status: 'OK',
    message: 'Thành Công',
    errors: null,
    student: {
      name_user: students.name_user,
      token: students.token,
    },
  };
};
module.exports.ErrorNotExistProfileUser = () => {
  return {
    status: 'ERROR',
    message: 'có lỗi xãy ra',
    errors: [
      {
        error_code: 'ERR_USR_005',
        message: 'Người Dùng Không Tồn Tại',
      },
    ],
    student: null,
  };
};
module.exports.ErrorExistProfileUser_SOCIAL = () => {
  return {
    status: 'ERROR',
    message: 'có lỗi xãy ra',
    errors: [
      {
        error_code: 'ERR_USR_006',
        message: 'Người Dùng đã Tồn Tại',
      },
    ],
    student: null,
  };
};
module.exports.dataProductItem_Success = (list) => {
  return {
    status: 'OK',
    message: 'Successs',
    errors: null,
    total_item: !list ?  0 : list.length,
    list_item: !list ?  null : [...list] ,
  };
};
module.exports.dataList_DaiLy_Success = (list) => {
  return {
    status: 'OK',
    message: 'Successs',
    errors: null,
    list_daily: !list ?  null : [...list] ,
  };
};
module.exports.dataList_HoaDon_Success = (list) => {
  return {
    status: 'OK',
    message: 'Successs',
    errors: null,
    list_hoadon: !list ?  null : [...list] ,
  };
};
module.exports.dataList_KhuyenMai_Success = (list) => {
  return {
    status: 'OK',
    message: 'Successs',
    errors: null,
    list_khuyenmai: !list ?  null : [...list] ,
  };
};
module.exports.dataList_ID_Success = (list) => {
  return {
    list_id: !list ?  null : list ,
  };
};
module.exports.dataList_ThuTien_Success = (list) => {
  return {
    status: 'OK',
    message: 'Successs',
    errors: null,
    list_phieuthutien: !list ?  null : [...list] ,
  };
};
module.exports.dataList_XH_Success = (list) => {
  return {
    status: 'OK',
    message: 'Successs',
    errors: null,
    list_phieu_xh: !list ?  null : [...list] ,
  };
};
module.exports.dataList_XH_ChiTiet_Success = (list_phieu_xh, list_hoadon) => {
  return {
    status: 'OK',
    message: 'Successs',
    errors: null,
    list_phieu_xh: !list_phieu_xh ?  null : [list_phieu_xh] ,
    list_hoadon: [list_hoadon]
  };
};
module.exports.dataProductItem_Errors = () => {
  return {
    status: 'ERROR',
    message: 'Có Lỗi Xảy Ra',
    errors: [
      {
        error_code: 'ERR_USR_004',
        message: 'Sản Phẩm Không Tồn Tại'
      },
    ],
    total_item: null,
    list_item: null ,
  };
};
