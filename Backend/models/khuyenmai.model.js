const mongoose = require('mongoose');
const { Schema } = mongoose;

const productsSchema = new Schema({
  maKM: {
    type: String
  },
  thoihan:{
    type: String
  },
  tilechietkhau:{
    type: String
  },
  ngaylapphieu:{
    type: String
  }
});
module.exports = mongoose.model('KHUYENMAI', productsSchema, 'KHUYENMAI');
