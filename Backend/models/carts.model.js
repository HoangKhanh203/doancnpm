const mongoose = require('mongoose');

const { Schema } = mongoose;

const carts = new Schema({
  id_carts: {
    type: String,
    require: true
  },
  listOrder:{
    type: Array,
    require: true
  }
});

module.exports = mongoose.model('CARTS', carts, 'CARTS');
