const mongoose = require('mongoose');

const { Schema } = mongoose;

const profileUser = new Schema({
  type: {
    type: String,
    require: true
  },
  id: {
    type: String,
    require: true
  },
  token: {
    type: String,
    require: true
  },
  name_user: {
    type: String,
    require: true
  },
  user_name: {
    type: String,
    require: true
  },
  carts: {
    type: String,
    require: true
  },
});

module.exports = mongoose.model('PROFILEUSERS', profileUser, 'PROFILEUSERS');
