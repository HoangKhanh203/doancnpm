const mongoose = require('mongoose');
const { Schema } = mongoose;

const productsSchema = new Schema({
    id_daily: {
    type: String
  },
  id_hoadon:{
    type: String
  },
  id_phieuthu:{
    type: String
  },
  ngaythutien:{
    type: String
  },
  tienthu:{
    type: String
  },
  conno:{
    type: String
  },
  maKM:{
    type: String
  }
});
module.exports = mongoose.model('PHIEUTHUTIEN', productsSchema, 'PHIEUTHUTIEN');
