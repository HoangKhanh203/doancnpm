const mongoose = require('mongoose');

const { Schema } = mongoose;

const ListDaily = new Schema({
  id_daily: {
    type: String,
    require: true,
  },
  name: {
    type: String,
    require: true,
  },
  type: {
    type: String,
    require: true,
  },
  district: {
    type: String,
    require: true,
  },
  manager: {
    type: String,
    require: true,
  },
  address: {
    type: String,
    require: true,
  },
  phone: {
    type: String,
    require: true,
  },
});
module.exports = mongoose.model('LISTDAILY', ListDaily, 'LISTDAILY');
