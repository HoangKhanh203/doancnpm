const mongoose = require('mongoose');
const { Schema } = mongoose;

const productsSchema = new Schema({
  id_daily: {
    type: String
  },
  id_hoadon:{
    type: String
  },
  id_phieuthu:{
    type: String
  },
  name:{
    type: String
  },
  date:{
    type: String
  },
  sum: {
      type: Object
  },
  list_product:{
    type: Array
}
});
module.exports = mongoose.model('HOADON', productsSchema, 'HOADON');
