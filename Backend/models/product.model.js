const mongoose = require('mongoose');

const { Schema } = mongoose;

const productsSchema = new Schema({
  nameProduct: {
    type: String,
    require: true,
  },
  ItemProduct: {
    type: Array,
    require: true,
  },
});
module.exports = mongoose.model('PRODUCTS', productsSchema, 'PRODUCTS');
