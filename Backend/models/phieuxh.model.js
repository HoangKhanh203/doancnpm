const mongoose = require('mongoose');
const { Schema } = mongoose;

const productsSchema = new Schema({
  id_phieuxuat: {
    type: String,
  },
  id_daily: {
    type: String,
  },
  id_hoadon: {
    type: String,
  },
  id_phieuthu: {
    type: String,
  },
  ngaylapphieu: {
    type: String,
  },
  maKM: {
    type: String,
  },
  tienthu: {
    type: String,
  },
  conno: {
    type: String,
  },
});
module.exports = mongoose.model('PHIEUXH', productsSchema, 'PHIEUXH');
