const Model_Event = require('./event.model');
const Model_Carts_Order = require('./carts_order.model');
const Model_Product = require('./product.model');
const Model_ListDaily = require('./list_daily');
const Model_ProfileUser = require('./profileUser.model');
const Model_Carts = require('./carts.model');
const Model_Hoa_Don = require('./hoadon.model');
const Model_Khuyen_Mai = require('./khuyenmai.model');
const Model_Phieu_Thu_Tien = require('./phieuthutien.model');
const Model_Phieu_XH = require('./phieuxh.model');
module.exports = {
  Model_Event,
  Model_Product,
  Model_ListDaily,
  Model_ProfileUser,
  Model_Carts_Order,
  Model_Carts,
  Model_Hoa_Don,
  Model_Khuyen_Mai,
  Model_Phieu_Thu_Tien,
  Model_Phieu_XH
};
