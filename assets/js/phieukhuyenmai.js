

let list_KM_main = [];



const handleThemKhuyenMai =async (valueData) => {
    const dataAPI =await CallAPI.ADEKM(valueData, "ADD");
    renderListKhuyenMai();
}
const handleSuaKhuyenMai=async (valueData) => {
    const dataAPI =await CallAPI.ADEKM(valueData, "EDIT");
    renderListKhuyenMai();
}
const handleXoaKhuyenMai =async (valueData) => {
    const dataAPI =await CallAPI.ADEKM(valueData, "DELETE");
    renderListKhuyenMai();
}

const selectKhuyenMai = (maKM) => {
    const arrInputKhuyenMai = document.querySelectorAll("#input-KM");
    const findmaKM = list_KM_main.find(item => item.maKM === maKM);
    arrInputKhuyenMai.forEach((itemKM) => {
        const itemInputKM = itemKM;
        itemInputKM.value = _.get(findmaKM,itemKM.dataset.type,'');
      });
}

const handleActionPhieuKhyenMai = (elmClick) => {
  const arrInputKhuyenMai = document.querySelectorAll("#input-KM");
  const objectInputKM = {};
 let checkValue = false;
  arrInputKhuyenMai.forEach((itemKM) => {
    const type = itemKM.dataset.type;
    const value = itemKM.value;
    console.log(type,value);

    if(type === 'tilechietkhau' || type ==="thoihan"){
        const parseValue = parseInt(value,10);
        console.log(parseValue)
        if (parseValue.toString() === "NaN" ) checkValue = true;
        else if (parseValue < 1)checkValue = true;
    } 
    _.set(objectInputKM, [type], value);
  });
  if(checkValue) return;
  switch (elmClick.dataset.type) {
    case "ADD":
      handleThemKhuyenMai(objectInputKM);
      break;
    case "DELETE":
      handleXoaKhuyenMai(objectInputKM);
      break;
    case "EDIT":
      handleSuaKhuyenMai(objectInputKM);
      break;
  }
};

const renderListKhuyenMai =async () => {
    const dataAPI =await CallAPI.getListKhuyenmai();
    const { getListKhuyenMai } = dataAPI;
    const { list_khuyenmai } = getListKhuyenMai;

    list_KM_main = [...list_khuyenmai];
    const elmToRender = renderElement.list_khuyenmai(list_khuyenmai);
     document.getElementById("body_list_item_khuyenmai").innerHTML = elmToRender.join("");
  const arrInputKhuyenMai = document.querySelectorAll("#input-KM");
  const objectInputKM = {};

  arrInputKhuyenMai.forEach((itemKM) => {
    const valueInput = itemKM;
    valueInput.value = null;
   
  });
  document.getElementsByClassName("ngaylapphieu")[0].value = today;
};