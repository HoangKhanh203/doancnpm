const renderElement = {
  list_daily: (list_daily) =>
    list_daily.map((item_list) => {
      if (!item_list?.id_daily) return;
      return `<tr>
                <td>${item_list.id_daily}</td>
                <td>${item_list.name}</td>
                <td>${item_list.type}</td>
                <td>${item_list.manager}</td>
                <td class="chucvu">${item_list.district}</td>
                <td class="diachi">${item_list.address}</td>
                <td class="sdt">${item_list.phone}</td>
                <td><button onClick="selectDaiLy('${item_list.id_daily}')">Chọn Đại Lý </button></td>

            </tr>`;
    }),
  list_hoadon: (list_hoadon, handle) =>
    list_hoadon.map((item_list) => {
      if (!item_list?.list_hoadon && !item_list?.id_hoadon) return;

      return `<tr>
                <td>${item_list.id_daily}</td>
                <td>${item_list.id_hoadon}</td>
                <td>${item_list.name}</td>
                <td>${item_list.date}</td>
                <td id="btnHandle">
                <button onClick="handleXoaHoaDon('${item_list.id_hoadon}')">Xóa Hóa Đơn </button>
                <button><a href="./HoaDon.html?mode=edit&id_hoadon=${item_list.id_hoadon}">Chi Tiết Hóa Đơn</a> </button>
                </td>
            </tr>`;
    }),
  list_khuyenmai: (list_khuyenmai) =>
    list_khuyenmai.map(
      (item_list) =>
        `<tr>
                <td>${item_list.maKM}</td>
                <td>${item_list.tilechietkhau}%</td>
                <td>${item_list.thoihan} ngày</td>
                <td>${item_list.ngaylapphieu}</td>
                <th><button id="btnHandle" onClick="selectKhuyenMai('${item_list.maKM}')">Chọn Mã Khuyến Mãi </button></th>
        </tr>`
    ),
    list_phieuthutien: (list_phieuthutien) =>
    list_phieuthutien.map(
      (item_list) =>
        `<tr>
                <td>${item_list.id_daily}</td>
                <td>${item_list.id_hoadon}</td>
                <td>${item_list.id_phieuthu}</td>
                <td>${item_list.ngaythutien}</td>
                <td>${item_list.maKM}</td>
                <td>${item_list.tienthu}</td>
                <td>${item_list.conno}</td>

                <th><button onClick="selectPhieuThuTien('${item_list.id_phieuthu}')">Chọn Phiếu Thu Tiền </button></th>
        </tr>`
    ),
    list_phieu_xh: (list_daily) =>
    list_daily.map((item_list) => {
      if (!item_list?.id_daily) return;
      return `<tr>
                <td>${item_list.id_phieuxuat}</td>
                <td>${item_list.id_daily}</td>
                <td>${item_list.id_hoadon}</td>
                <td>${item_list.id_phieuthu}</td>
                <td class="chucvu">${item_list.ngaylapphieu}</td>
                <td class="chucvu">${item_list.maKM}</td>
                <td class="diachi">${item_list.tienthu}</td>
                <td class="sdt">${item_list.conno}</td>
                <th id="btnHandle">
                  <button onClick="selectPhieuXH('${item_list.id_phieuxuat}')">Chọn Phiếu Xuất </button>
                  <button onClick="chitietPXH('${item_list.id_phieuxuat}')">Chi Tiết Phiếu Xuất </button>
                </th>

            </tr>`;
    }),
};
