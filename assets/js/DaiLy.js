let ListDaiLyMain = [];
const showInputSearch = () => {
    document.getElementById("text-search").style.display = "none";
    document.getElementById("input-search").style.display = "block";
}

const selectDaiLy = (id_daily) => {
    const wrapArr = document.querySelector("#arr_input")
    const arrInputKhuyenMai = wrapArr.querySelectorAll("input");
    const findmaKM = ListDaiLyMain.find(item => item.id_daily === id_daily);
    arrInputKhuyenMai.forEach((itemDaily) => {
        const itemInputKM = itemDaily;
        itemInputKM.value = _.get(findmaKM,itemDaily.dataset.type,'');
      });
}
const regSearch = (id_body,listArr) => {

    const inputSearch = document.getElementById("input-search")
    const valueInput = inputSearch.value;
    if(valueInput.length < 2) {
        renderListDaiLy_Content(listArr)
        return;
    };
    const optionsSearch = document.getElementById('select_search').value;
    console.log(optionsSearch)
    const listDaiLySearch = listArr.filter(item => {
        const reg = new RegExp(valueInput,'g');
        return reg.exec(_.get(item,optionsSearch, ''))
    })
    let arrDaiLySearch = [ ...listDaiLySearch]
    if(arrDaiLySearch.length < 1) arrDaiLySearch = [...listArr];

    const elmToRender = renderElement.list_daily(arrDaiLySearch);
    document.getElementById(id_body).innerHTML = elmToRender.join("");
}
const search_value = (id_body) => {
    regSearch("body_list_item_daily",ListDaiLyMain);
}

const renderListDaiLy_Content = (_list_daily) => {
    const elmToRender = renderElement.list_daily(_list_daily);
    document.getElementById("body_list_item_daily").innerHTML = elmToRender.join("");
}
const renderListDaiLy = async () => {
    const dataAPI = await CallAPI.getListDaiLy();
    const { list_daily =[] } = dataAPI.getListDaily;
    if(list_daily.length < 1) return;
    ListDaiLyMain = [...list_daily];
    renderListDaiLy_Content(ListDaiLyMain)
}

const handleThemDaiLy = async (elmProps) => {
    const dataAPI = await CallAPI.ADEDaiLy(elmProps,'ADD');
    ListDaiLyMain = [{...elmProps}, ...ListDaiLyMain]
    renderListDaiLy_Content(ListDaiLyMain)
}
const handleXoaDaiLy = async (elmProps) => {
    const dataAPI = await CallAPI.ADEDaiLy(elmProps,'DELETE');
    renderListDaiLy();

}
const handleSuaDaiLy = async (elmProps) => {
    const dataAPI = await CallAPI.ADEDaiLy(elmProps,'EDIT');
    renderListDaiLy();
}

const handleActionDaiLy = (elm) => {
    console.log('co');
    const elmArrInput = document.querySelector("#arr_input");
    if (window.getComputedStyle(elmArrInput, null).display === "none") {
        return elmArrInput.style.display = "table-row"
    }
    else {
        elmArrInput.style.display = "none"
        const arrInput = elmArrInput.querySelectorAll("input");
        const arrInputObject = [];
        for(let index = 0 ; index < arrInput.length ; index++){
            const type = arrInput[index].dataset.type;
            const value = arrInput[index].value;
            const objectInput = `{"${type}":"${value}"}`
            arrInputObject.push(JSON.parse(objectInput))
        }
        const formatObject = arrInputObject.reduce(function(result, item) {
            var key = Object.keys(item)[0];
            result[key] = item[key];
            return result;
          }, {});
        switch (elm.dataset.type) {
            case "ADD":
                return handleThemDaiLy(formatObject)
            case "DELETE":
                return handleXoaDaiLy(formatObject)
            case "EDIT":
                return handleSuaDaiLy(formatObject)
        }
    }
}

let listID = {};
const getID = async () => {
    const data = await CallAPI.getID();
    const {list_id } = data.getID
    listID = {...list_id}
}
getID();
