const layoutLeft =
    `
<div class="sidebar-wrapper">
<div class="logo">
    <a href="./List_DaiLy.html" class="simple-text">
        Quản Lý Đại Lý
    </a>
</div>
<ul class="nav">
    <li class="nav-item" id="List_DaiLy">
        <a class="nav-link" href="List_DaiLy.html">
            <i class="nc-icon nc-chart-pie-35"></i>
            <p>Danh Sách Đại Lý</p>
        </a>
    </li>
    <li class="nav-item" id="HoaDon">
        <a class="nav-link" href="HoaDon.html">
            <i class="nc-icon nc-circle-09"></i>
            <p>Hóa Đơn</p>
        </a>
    </li>
    <li  class="nav-item" id="list_HD">
        <a class="nav-link" href="list_HD.html">
            <i class="nc-icon nc-notes"></i>
            <p>Danh Sách Hóa Đơn</p>
        </a>
    </li>
    <li class="nav-item" id="PhieuKhuyenMai">
        <a class="nav-link" href="PhieuKhuyenMai.html">
            <i class="nc-icon nc-paper-2"></i>
            <p>Lập Phiếu Khuyến Mãi</p>
        </a>
    </li>
    <li class="nav-item" id="PhieuThuTien">
        <a class="nav-link" href="PhieuThuTien.html">
            <i class="nc-icon nc-atom"></i>
            <p>Lập Phiếu Thu Tiền</p>
        </a>
    </li>
    <li class="nav-item" id="PhieuXH">
        <a class="nav-link" href="PhieuXH.html">
            <i class="nc-icon nc-pin-3"></i>
            <p>Lập phiếu Xuất Hàng</p>
        </a>
    </li>
</ul>
</div>
`;
document.getElementById('sidebar').innerHTML = layoutLeft;

const listScript = [
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/CallApi.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/DaiLy.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/elementToRender.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/core/jquery.3.2.1.min.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/core/popper.min.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/core/bootstrap.min.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/plugins/bootstrap-switch.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/plugins/chartist.min.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/plugins/bootstrap-notify.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/light-bootstrap-dashboard.js?v=2.0.0 " },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/demo.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/helper.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "https://cdn.jsdelivr.net/npm/lodash@4.17.11/lodash.min.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/HoaDon.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/phieukhuyenmai.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/phieuthutien.js" },
    { tag: 'script', attri: "src", valueType: 'type', type: 'text/javascript', src: "../assets/js/phieuxuathang.js" },


    
    { tag: 'link', attri: "href",  valueType: 'rel', type: 'stylesheet', src:"https://fonts.googleapis.com/css?family=Montserrat:400,700,200" },
    { tag: 'link', attri: "href",  valueType: 'rel', type: 'stylesheet', src:"https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" },
    { tag: 'link', attri: "href",  valueType: 'rel', type: 'stylesheet', src:"../assets/css/bootstrap.min.css" },
    { tag: 'link', attri: "href",  valueType: 'rel', type: 'stylesheet', src:"../assets/css/light-bootstrap-dashboard.css?v=2.0.0 " },
    { tag: 'link', attri: "href",  valueType: 'rel', type: 'stylesheet', src:"../assets/css/demo.css" },
    { tag: 'link', attri: "href",  valueType: 'rel', type: 'stylesheet', src:"../assets/css/list_daily.css" },
    { tag: 'link', attri: "href",  valueType: 'rel', type: 'stylesheet', src:"../assets/css/hoadon.css" },
    { tag: 'link', attri: "href",  valueType: 'rel', type: 'stylesheet', src:"../assets/css/phieukhuyenmai.css" },
    { tag: 'link', attri: "href",  valueType: 'rel', type: 'stylesheet', src:"../assets/css/phieuthutien.css" },

    { tag: 'link', attri: "href",  valueType: 'rel', type: 'stylesheet', src:"../assets/css/phieuxuathang.css" },


];
let htmlListScript = '';
for (let item in listScript) {
    const script = document.createElement(listScript[item].tag);
    script.setAttribute(listScript[item].attri, listScript[item].src);
    script.setAttribute(listScript[item].valueType, listScript[item].type);
    document.head.appendChild(script)
}
