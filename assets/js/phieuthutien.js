let list_phieuthutien_main = [];
const handleThemPhieuThuTien = async (valueInput) => {
    const dataAPI = await CallAPI.ADEPhieuThuTien(valueInput,'ADD');
    loadPhieuThuTien()
}

const handleXoaPhieuThuTien =async (valueInput) => {
    const dataAPI = await CallAPI.ADEPhieuThuTien(valueInput,'DELETE');
    loadPhieuThuTien()
}
const handleSuaPhieuThuTien =async (valueInput) => {
    const dataAPI = await CallAPI.ADEPhieuThuTien(valueInput,'EDIT');
    loadPhieuThuTien()
}
const selectPhieuThuTien = (id_phieuthu) => {
    const arrInputKhuyenMai = document.querySelectorAll("#input-phieuthutien");
    const findmaKM = list_phieuthutien_main.find(item => item.id_phieuthu === id_phieuthu);
    arrInputKhuyenMai.forEach((itemKM) => {
        const itemInputKM = itemKM;
        itemInputKM.value = _.get(findmaKM,itemKM.dataset.type,'');
      });
  document.querySelector(".list_button").querySelector("button").style.display ="none"

}
const loadID_Daily = (elm) =>{
    const value = elm.value;
    const valueDaiLy = ListHoaDonMain.find(item => item.id_hoadon === value);
  document.querySelector(".id_daily").value = valueDaiLy.id_daily;
  console.log(valueDaiLy);
  document.getElementById("sotienmuahang").innerText = valueDaiLy.sum.total_price+ " vnđ";
  document.getElementById("tenhoadon").innerText = valueDaiLy.name;

}
const loadPhieuThuTien = async () => {
    const dataAPI =await CallAPI.getListPhieuThuTien();
    const { getListPhieuThuTien } = dataAPI;
    const { list_phieuthutien } = getListPhieuThuTien;
    list_phieuthutien_main = [...list_phieuthutien]
    const elmToRender = renderElement.list_phieuthutien(list_phieuthutien);
    document.getElementById("body_list_item_phiethutien").innerHTML = elmToRender.join("");

}
const loadPhieuThuTien_content = async () => {
    const data = await CallAPI.getListHoaDon();
    const {list_hoadon } = data.getListHoaDon
    ListHoaDonMain = [...list_hoadon];

    const arrIDHoaDon = [];
    if(list_hoadon.length < 1) return;
    list_hoadon.forEach(item => arrIDHoaDon.push(item));
  const htmlHoaDon = arrIDHoaDon.map(item => `<option value="${item.id_hoadon}">${item.id_hoadon}</option>`)

  document.querySelector(".id_daily").value = arrIDHoaDon[0].id_daily;
  document.getElementById("select_hoadon").innerHTML = htmlHoaDon.join();
  document.getElementsByClassName("ngaythutien")[0].value = today;
  document.getElementById("sotienmuahang").innerText = arrIDHoaDon[0].sum.total_price + " vnđ"
  document.getElementById("tenhoadon").innerText = arrIDHoaDon[0].name;
  loadPhieuThuTien()
}
const search_phieuthutien  = () => {
    const inputSearch = document.getElementById("input-search")
    const valueInput = inputSearch.value;
    if(valueInput.length < 2) {
        loadPhieuThuTien()
        return;
    };
    const optionsSearch = document.getElementById('select_search').value;
    console.log(optionsSearch);

    const listDaiLySearch = list_phieuthutien_main.filter(item => {
        const reg = new RegExp(valueInput,'g');
        return reg.exec(_.get(item,optionsSearch, ''))
    })
    let arrDaiLySearch = [ ...listDaiLySearch]
    if(arrDaiLySearch.length < 1) arrDaiLySearch = [...list_phieuthutien_main];
    const elmToRender = renderElement.list_phieuthutien(arrDaiLySearch);
    document.getElementById("body_list_item_phiethutien").innerHTML = elmToRender.join("");
}
const handleActionPhieuThuTien = (elm) => {
  document.querySelector(".list_button").querySelector("button").style.display ="inline"

        const arrInput = document.querySelectorAll("#input-phieuthutien");
        let checkvalue = false;
        const formatObject = {};
        for(let index = 0 ; index < arrInput.length ; index++){
            const type = arrInput[index].dataset.type;
            const value = arrInput[index].value;
            if(!value && (type!=="id_phieuthu" && type !=="maKM")){
                console.log(type);
                checkvalue =true
                break;
            };
            _.set(formatObject, [type], value);
            if(type !=="ngaythutien") arrInput[index].value = ""
        }
        const phieuthu = document.getElementById("select_hoadon").value;
        _.set(formatObject, ["id_hoadon"],phieuthu);
        if(checkvalue) return;
          switch (elm.dataset.type) {
            case "ADD":
                return handleThemPhieuThuTien(formatObject)
            case "DELETE":
                return handleXoaPhieuThuTien(formatObject)
            case "EDIT":
                return handleSuaPhieuThuTien(formatObject)
        }

}
