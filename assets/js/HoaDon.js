let ListHoaDonMain = [];
let listSPMain = [];
var today = new Date();
var dd = String(today.getDate()).padStart(2, "0");
var mm = String(today.getMonth() + 1).padStart(2, "0"); //January is 0!
var yyyy = today.getFullYear();

today = dd + "/" + mm + "/" + yyyy;

const renderListHoaDon = async (id_hoadon = "") => {
  const { getListHoaDon } = await CallAPI.getListHoaDon(id_hoadon);
  const { list_hoadon = [] } = getListHoaDon;

  if (list_hoadon.length < 1) return;
  ListHoaDonMain = [...list_hoadon];
  renderListHoaDon_Content(ListHoaDonMain);
};
const renderListHoaDon_Content = (_list_hoadon) => {
  const elmToRender = renderElement.list_hoadon(_list_hoadon);
  document.getElementById("body_list_item_hoadon").innerHTML = elmToRender.join(
    ""
  );
};
const loadInforHoaDon = async () => {
  const mode = getParameterByName("mode");
  const id_hoadon = getParameterByName("id_hoadon");
  document.getElementById("date-now").value = today;
  const data = await CallAPI.getID();
    const {list_id } = data.getID
    listID = {...list_id}
  const html = listID.id_daily.map(item => `<option value="${item}">${item}</option>`)
  document.getElementById("select_daily").innerHTML = html.join();

  
  if (mode) {
    document.getElementById("editHoaDon").style.display = "block";
    document.getElementById("addHoaDon").style.display = "none";
    const maKMArr = document.querySelectorAll("#kuyenmaiSP");
    maKMArr.forEach(item => item.style.display ="none")

    const queryInFor = document.querySelector(".input-infor-hoadon");
    const { getListHoaDon } = await CallAPI.getListHoaDon(id_hoadon);
    const { list_hoadon = [] } = getListHoaDon;

    let htmlSP = "";
    document.getElementById("body_list_item_hoadon").innerHTML = htmlSP;

    const hoadon = list_hoadon.find(item => item.id_hoadon === id_hoadon);

    const indexDaiLy = listID.id_daily.findIndex(item =>item === hoadon.id_daily);
  document.getElementById("select_daily").selectedIndex = listID.id_daily.findIndex(item =>item === hoadon.id_daily);


    let total_money =total_amount = 0;
      total_money = _.get(hoadon,"sum.total_price",0)
      total_amount = _.get(hoadon,"sum.amount",0)

      const arrInput = queryInFor.querySelectorAll("input");

      const { list_product } = hoadon;
      arrInput.forEach(item => {
        const inputType = item;
        inputType.value = _.get(hoadon,inputType.dataset.type);
      })
      list_product.forEach((item, index) => {
        htmlSP += `
        <tr id="input-sp">
              <th width="20"><input   value=${index+1} id="stt-sp" data-type="stt" disabled /></th>
              <th><input data-type="name"          type=""       value="${_.get(
                item,
                "name",
                ""
              )}"    onkeyup="changeInputSP(this, ${index + 1})" /></th>
              <th><input data-type="dvtinh"        type=""       value="${_.get(
                item,
                "dvtinh",
                ""
              )}"  onkeyup="changeInputSP(this, ${index + 1})" /></th>
              <th><input data-type="dongia"        type="number" value="${_.get(
                item,
                "dongia",
                ""
              )}"  onkeyup="changeInputSP(this, ${index + 1})" /></th>
              <th><input data-type="soluong"       type="number" value="${_.get(
                item,
                "soluong",
                ""
              )}"                    onkeyup="changeInputSP(this, ${
          index + 1
        })" /></th>
              <th><input data-type="total_price" id="total_price"  value="${_.get(
                item,
                "total_price",
                ""
              )}"                  onkeyup="changeInputSP(this, ${
          index + 1
        })" disabled /></th>
              <th>
              <button onClick="removeItem(${index + 1})">Xóa</button>
              </th>
          </tr>
    `;
      });
    document.getElementById("body_list_item_hoadon").innerHTML = htmlSP;
    document.getElementById("total_money").innerHTML = total_money;
    document.getElementById("total_amount").innerHTML = total_amount;
  } else {
    addProductItem();
  }
};

const search_hoadon = () => {
  const inputSearch = document.getElementById("input-search");
  const valueInput = inputSearch.value;
  if (valueInput.length < 2) {
    const elmToRender = renderElement.list_hoadon(ListHoaDonMain);
    document.getElementById("body_list_hoadon").innerHTML = elmToRender.join(
      ""
    );
    return;
  }
  const optionsSearch = document.getElementById("select_search").value;
  const listDaiLySearch = ListHoaDonMain.filter((item) => {
    const reg = new RegExp(valueInput, "g");
    return reg.exec(_.get(item, optionsSearch, ""));
  });
  let arrDaiLySearch = [...listDaiLySearch];
  const elmToRender = renderElement.list_hoadon(arrDaiLySearch);
  document.getElementById("body_list_hoadon").innerHTML = elmToRender.join("");
};

const displayInput = (_input_infor, displayStyle) =>
  _input_infor.forEach((item) => (item.style.display = displayStyle));

const loadListSP = (_listSPMain = listSPMain) => {
  let htmlSP = "";
  let total_money = (total_amount = 0);
  _listSPMain.forEach((item, index) => {
    const total_price = _.get(item, "total_price", "");
    const amount = _.get(item, "soluong", 0);
    console.log(item);
    total_money += parseInt(total_price, 10);
    total_amount += parseInt(amount, 10);
    htmlSP += `
    <tr id="input-sp">
        <th width="20"><input   value=${
          index + 1
        } id="stt-sp" data-type="stt" disabled /></th>
        <th><input data-type="name"          type=""       value="${_.get(
          item,
          "name",
          ""
        )}"    onkeyup="changeInputSP(this, ${index + 1})" /></th>
        <th><input data-type="dvtinh"        type=""       value="${_.get(
          item,
          "dvtinh",
          ""
        )}"  onkeyup="changeInputSP(this, ${index + 1})" /></th>
        <th><input data-type="dongia"        type="number" value="${_.get(
          item,
          "dongia",
          ""
        )}"  onkeyup="changeInputSP(this, ${index + 1})" /></th>
        <th><input data-type="soluong"       type="number" value="${amount}"                    onkeyup="changeInputSP(this, ${
      index + 1
    })" /></th>
        <th><input data-type="total_price" id="total_price"  type=""       value="${total_price}"               onkeyup="changeInputSP(this, ${
      index + 1
    })" disabled /></th>
        <th>
        <button onClick="removeItem(${index + 1})">Xóa</button>
        </th>
    </tr>
        `;
  });
  document.getElementById("total_money").innerHTML = total_money;
  document.getElementById("total_amount").innerHTML = total_amount;

  document.getElementById("body_list_item_hoadon").innerHTML =
    htmlSP +
    '<tr> <th  colspan="7" id="addSanPham"><button onclick="addProductItem()">Thêm Sản Phẩm</button></th></tr>';
};
// const tinhtienKM = (money) => {
//   const valueInput = document.getElementById("maKMINPUT").value;
//   let objectMaKM = {};
//   listID.maKM.forEach(item => {
//     if(valueInput === item.maKM)
//       objectMaKM = {...item};
//   })
//   if(!objectMaKM.maKM) return money;
//   console.log(!objectMaKM);

//   return money - ((objectMaKM.tilechietkhau*money)/100)
// }
const addProductItem = () => {
  const newSP = [...listSPMain];
  newSP.push({
    total_price: 0,
    dongia: 0,
    soluong: 0,
    dvtinh: "",
    name: "",
  });
  listSPMain = [...newSP];
  loadListSP(listSPMain);
};
const changeInputSP = (ref, stt) => {
  const arrSP = [...listSPMain];
  const sttRef = stt - 1;
  const type = ref.dataset.type;
  const value = parseInt(ref.value, 10);
  const soluong = parseInt(_.get(arrSP[sttRef], "soluong", 0), 10);
  const dongia = parseInt(_.get(arrSP[sttRef], "dongia", 0), 10);

  let total_price = dongia * soluong;
  if (type === "soluong") total_price = value * dongia;
  else if (type === "dongia") total_price = value * soluong;
  console.log(total_price);
  if (total_price < 0 || total_price == NaN) return;
  _.set(arrSP, [sttRef, type], ref.value);
  if (total_price > 0) {
    _.set(arrSP, [sttRef, "total_price"], total_price);

    let total_money = 0;
    let total_amount = 0;
    arrSP.forEach((item, index) => {
      const total_price = _.get(item, "total_price", "");
      const amount = _.get(item, "soluong", 0);

      total_money += parseInt(total_price, 10);
      total_amount += parseInt(amount, 10);
    });
    document.getElementById("total_money").innerHTML = total_money;
    document.getElementById("total_amount").innerHTML = total_amount;
    document.querySelectorAll("#total_price")[sttRef].value = total_price;
  }

  return (listSPMain = [...arrSP]);
};
const removeItem = (stt) => {
  if (listSPMain.length === 0) return;
  const _listSP = [...listSPMain];

  _listSP.splice(stt - 1, 1);
  listSPMain = [..._listSP];
  loadListSP(listSPMain);
};
const handleThemHoaDon = async (data) => {
  const dataAPI = await CallAPI.ADEHoaDon(data, "ADD");
  // renderListDaiLy_Content(ListDaiLyMain)
};
const handleSuaHoaDon = async (data) => {
  const dataAPI = await CallAPI.ADEHoaDon(data, "EDIT");
}
const handleXoaHoaDon = async (idhoadon) => {
  const dataAPI = await CallAPI.ADEHoaDon({ id_hoadon: idhoadon }, "DELETE");
  loadListHoadon();
};
const loadListHoadon = async () => {
  const dataAPI = await CallAPI.getListHoaDon();
  const { list_hoadon } = dataAPI.getListHoaDon;
  console.log(list_hoadon);
  if(list_hoadon.length < 1) return
  ListHoaDonMain = [...list_hoadon];
  const elmToRender = renderElement.list_hoadon(list_hoadon);
  document.getElementById("body_list_hoadon").innerHTML = elmToRender.join("");
};
const handleActionHoaDon = (elm) => {
  const input_infor = document.querySelectorAll("#input-infor");
  const elmArrInput = document.querySelector("#body_list_item_hoadon");
  const buttonAddSP = document.getElementById("addSanPham");
  const listInputSP = document.querySelectorAll("#input-sp");
  if (window.getComputedStyle(elmArrInput, null).display === "none") {
    const lengthSP = listInputSP.length;

    addProductItem();

    elmArrInput.style.display = "table-row-group";
    displayInput(input_infor, "block");
    return;
  } else {
    
    const elmArrInputSP = document.querySelector(".input-infor-hoadon");
    const arr = document.querySelectorAll("#input-sp");

    const arrInput = elmArrInputSP.querySelectorAll("input");
    const arrSP = [];
    const InputObject = {};
    let valueSP = 0;
    const arrAfterFilter = [];
    const filterArrSP = arr.forEach(item => {
      const value = item.querySelectorAll("input");
      console.log(value[1].value); 
      if(!value[1].value  || !value[2].value || !value[3].value || !value[4].value){
        return;
      }
        arrAfterFilter.push(item)
      })
    console.log(arrAfterFilter);
      
    for (let index = 0; index < arrAfterFilter.length; index++) {
      const arrInputSP = arrAfterFilter[index].querySelectorAll("input");

      for (let index2 = 0; index2 < arrInputSP.length; index2++) {
        const type = arrInputSP[index2].dataset.type;
        const value = arrInputSP[index2].value;
          _.set(arrSP, [index, type], value);
      }
    }
    const objectSP = {};
    for (let index = 0; index < arrInput.length; index++) {
      const type = arrInput[index].dataset.type;
      const value = arrInput[index].value;
      
      _.set(objectSP, [type], value);
    }
    const daily = document.getElementById("select_daily").value;
    let valueMoney = document.getElementById("total_money").textContent;
    valueMoney = parseInt(valueMoney);
    if(valueMoney === 0) return;
    _.set(objectSP, ["id_daily"], daily);
    _.set(objectSP, ["sum"],valueMoney.toString() );
    objectSP.list_product = arrSP;
    switch (elm.dataset.type) {
      case "ADD":
        handleThemHoaDon(objectSP);
        break;
      case "EDIT":
        handleSuaHoaDon(objectSP);
        break;
    }
    displayInput(input_infor, "none");
    elmArrInput.style.display = "none";
    if(buttonAddSP){
      buttonAddSP.style.display = "none";
    }
    window.location.href = urlmain + "list_HD.html";
  }
  
};
const edit_hoadon = () => {

}
const delete_hoadon = (idhoadon) => {
  handleXoaHoaDon(idhoadon);
};
