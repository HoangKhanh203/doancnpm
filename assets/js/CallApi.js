let queryProducts = `query{
  products{
    name
    image
    description
  }
}`;
var CallAPI = {
login: (signInInput) => {
  const mutationLogin = `mutation(
      $user_name: String!
      $password: String!
      $remember_me: Boolean
    ) {
      signIn(
        input: {
          user_name: $user_name
          password: $password
          remember_me: $remember_me
        }
      ) {
        status
        errors {
          error_code
          message
        }
        auth_token 
      }
    }`;
  const variablesSignIn = {
    user_name: signInInput.user_name,
    password: signInInput.password,
    remember_me: signInInput.remember_me,
  };
  return API(mutationLogin, variablesSignIn);
},
getProfile: (inputProfile) => {
  const mutaionGetProfile = `
   mutation(
            $type: Profile_Type!
            $token: String
            $id: String
            $user_name: String
            $name_user: String
            ) {
    getInforUser(input:{
                        type: $type
                        token: $token
                        id: $id
                        user_name: $user_name
                        name_user:  $name_user    
    }){
      status
      message
      errors{
        error_code
        message
      }
      student{
        name_user
        token
      }
    }
  }
    `;
  const variablesGetProfile = {
    type: inputProfile.type,
    token: inputProfile.token,
    id: inputProfile.id,
    user_name: inputProfile.user_name,
    name_user: inputProfile.name_user,
  };
  return API(mutaionGetProfile, variablesGetProfile);
},
signUp: (signUpInput) => {
  const mutationSignUp = `
  mutation (
    $user_name: String!
    $password: String!
    $name_user: String!
  ){
    signUp(input:{user_name: $user_name,password: $password,name_user: $name_user}){
          status
      message
      errors{
        error_code
        message
      }
      auth_token
    }
  }
  `;
  const variablesSignUp = {
    user_name: signUpInput.user_name_up,
    password: signUpInput.password_up,
    name_user: signUpInput.your_name_up,
  };
  return API(mutationSignUp, variablesSignUp);
},
getItemProduct: (nameProduct) => {
  const mutationGetItemTopSale = `
  query($nameProduct: String!){
    getDataProduct(input:{nameProduct:$nameProduct}){
      status
      message
      errors{
        error_code
        message
      }
      total_item
      list_item{
        id
        title
        price
        image
        imageSec
        amount
        forphone
        type
        ranking
        trademark
      }
    }
  }
  `;
  const variablesgetItemProduct = {
    nameProduct,
  };
  return API(mutationGetItemTopSale, variablesgetItemProduct);
},
getListDaiLy: () => {
  const getListDaiLy = `
  query{
    getListDaily{
      status
      message
      errors {
        message
        error_code
      }
      list_daily{
        id_daily
        name
        type
        district
        manager
        address
        phone
      }
      
    }
  }
  `;
  const variablesgetIListDaily = {
    getListDaiLy,
  };
  return API(getListDaiLy, variablesgetIListDaily);
},
getListPhieuXH: () => {
  const getListDaiLy = `
  query{
    getListPhieuXH{
      status
      message
      errors {
        message
        error_code
      }
      list_phieu_xh{
        id_phieuxuat
        id_daily
        id_hoadon
        id_phieuthu
        ngaylapphieu
        maKM
        tienthu
        conno
      }
      
    }
  }
  `;

  return API(getListDaiLy);
},
getItemID: (id_item) => {
  const mutationGetItemID = `
  query($id_item: [String!]){
    getItemId(input:{id_item:$id_item}){
      status
      message
      errors{
        error_code
        message
      }
      list_item{
          id
          title
          price
          image
          imageSec
          amount
          forphone
          type
          ranking
          trademark
      }
  }
  }
  `;
  const variablesgetItemID = {
    id_item,
  };
  return API(mutationGetItemID, variablesgetItemID);
},

searchItem: (valueSearch) => {
  const mutationSearch = `
  mutation($valueSearch:String!){
    searchItem(input: $valueSearch){
      status
      message
      errors{
        error_code
        message
      }
      total_item
      list_item{
            id
            title
            price
            image
            imageSec
            amount
            forphone
            type
            ranking
      }
    }
  }
  `;
  const variablesValueSearch = {
    valueSearch,
  };
  return API(mutationSearch, variablesValueSearch);
},
ADEDaiLy: (valueInputDaily, type) => {
  const mutationADEDaiLy = `
  mutation($valueInputDaily:DaiLyInfor, $type: ADEDaiLyType! ){
    ADEDaiLy(input: {valueInputDaily: $valueInputDaily, type: $type}){
      status
      message
      errors{
        error_code
        message
      }
    }
  }
  `;
  const variablesDaiLy = {
    valueInputDaily,
    type,
  };
  return API(mutationADEDaiLy, variablesDaiLy);
},
ADEPhieuXH: (valueInputPhieuXH, type) => {
  const mutationADEPhieuXH = `
  mutation($valueInputPhieuXH:PhieuXHInfor, $type: ADEDaiLyType! ){
    ADEPhieuXH(input: {valueInputPhieuXH: $valueInputPhieuXH, type: $type}){
      status
      message
      errors{
        error_code
        message
      }
    }
  }
  `;
  const variablesPhieuXH = {
    valueInputPhieuXH,
    type,
  };
  return API(mutationADEPhieuXH, variablesPhieuXH);
},
ADEHoaDon: (valueInputHoaDon, type) => {
  const mutationADEHoaDon = `
  mutation($valueInputHoaDon:HoaDonInfor, $type: ADEDaiLyType! ){
    ADEHoaDon(input: {valueInputHoaDon: $valueInputHoaDon, type: $type}){
      status
      message
      errors{
        error_code
        message
      }
    }
  }
  `;
  const variablesHoaDon = {
    valueInputHoaDon,
    type,
  };
  return API(mutationADEHoaDon, variablesHoaDon);
},
getListHoaDon: (id_hoadon) => {
  const mudationListHoaDon = `
    query($id_hoadon : String){
      getListHoaDon(input: $id_hoadon ){
        status
        message
        errors{
          message
          error_code
        }
        list_hoadon {
          id_daily
          id_hoadon
          id_phieuthu
          name
          date
          sum {
            amount
            total_price
          }
          list_product{
            dongia
            dvtinh
            name
            soluong
            stt
            total_price
          }
        }
      }
    }
  
  `;
  const valueHoaDon = {
    id_hoadon,
  };
  return API(mudationListHoaDon, valueHoaDon);
},
getItemPhieuXuatHang: (id_phieuxuat) => {
  const getItemPhieuXuatHang = `
  query($id_phieuxuat : String){
    getItemPhieuXuatHang(input: $id_phieuxuat ){
      status
      message
      list_hoadon{
        id_daily
        id_hoadon
        id_phieuthu
        name
        date
        sum{
          amount
        total_price
        }
        list_product{
          dongia
          dvtinh
          name
          soluong
          stt
          total_price
        }
      }
      list_phieu_xh{
        id_phieuxuat
        id_daily
        id_hoadon
        id_phieuthu
        ngaylapphieu
        maKM
        tienthu
        conno
      }
  }
}

`;
  const valuePXHn = {
    id_phieuxuat,
  };
  return API(getItemPhieuXuatHang, valuePXHn);
},
getListKhuyenmai: (maKM) => {
  const mudationListKhuyenMai = `
    query($maKM : String){
      getListKhuyenMai(input: $maKM ){
        status
        message
        errors{
          message
          error_code
        }
        list_khuyenmai {
          maKM
          thoihan
          tilechietkhau
          ngaylapphieu
        }
      }
    }
  
  `;
  const valueKhuyenMai = {
    maKM,
  };
  return API(mudationListKhuyenMai, valueKhuyenMai);
},
ADEKM: (valueInputKM, type) => {
  const mutationADEKM = `
      mutation($valueInputKM:KMInfor, $type: ADEDaiLyType! ){
        ADEKM(input: {valueInputKM: $valueInputKM, type: $type}){
          status
          message
          errors{
            error_code
            message
          }
        }
      }
`;
  const variablesKM = {
    valueInputKM,
    type,
  };
  return API(mutationADEKM, variablesKM);
},
ADEPhieuThuTien: (valueInputPTT, type) => {
  const mutationADEKM = `
      mutation($valueInputPTT:PTTInfor, $type: ADEDaiLyType! ){
        ADEPTT(input: {valueInputPTT: $valueInputPTT, type: $type}){
          status
          message
          errors{
            error_code
            message
          }
        }
      }
      `;
  const variablesKM = {
    valueInputPTT,
    type,
  };
  return API(mutationADEKM, variablesKM);
},
getListPhieuThuTien: () => {
  const mudationListPhieuThuTien = `
    query{
      getListPhieuThuTien{
        status
        message
        errors{
          message
          error_code
        }
        list_phieuthutien {
          id_daily
          id_hoadon
          id_phieuthu
          ngaythutien
          tienthu
          maKM
          conno
        }
      }
    }
  
  `;
  return API(mudationListPhieuThuTien);
},
getID: () => {
  const queryID = `
    query{
      getID{
        list_id {
          id_daily
          id_hoadon
          maKM {
            maKM
            thoihan
            tilechietkhau
            ngaylapphieu
          }
          id_phieuthu
          id_phieuxuat
        }
      }
    }
  
  `;
  return API(queryID);
},
};
async function API(queryType, variablesInput) {
let data_CallAPI = await fetch("http://localhost:8080/graphql/", {
  method: "POST",
  headers: { "Content-Type": "application/json" },
  body: JSON.stringify({
    query: queryType,
    variables: {
      ...variablesInput,
    },
  }),
})
  .then((res) => res.json())
  .then((res) => {
    return res.data;
  })
  .catch((err) => console.log(err));
return data_CallAPI;
}
