let ListPhieuXHMain = []
const renderListPhieuXH_Content = (_list_daily) => {
    const elmToRender = renderElement.list_phieu_xh(_list_daily);
    document.getElementById("body_list_item_phieuxh").innerHTML = elmToRender.join("");
}
const renderListPhieuXH = async () => {


    const dataAPI = await CallAPI.getListPhieuXH();

    const { list_phieu_xh =[] } = dataAPI.getListPhieuXH;
    if(list_phieu_xh.length < 1) return;
    ListPhieuXHMain = [...list_phieu_xh];
    renderListPhieuXH_Content(ListPhieuXHMain);

}
const search_phieuxuathang = () => {
    const inputSearch = document.getElementById("input-search")
    const valueInput = inputSearch.value;
    if(valueInput.length < 2) {
        renderListPhieuXH_Content(ListPhieuXHMain)
        return;
    };
    const optionsSearch = document.getElementById('select_search').value;
    const listDaiLySearch = ListPhieuXHMain.filter(item => {
        const reg = new RegExp(valueInput,'g');
        return reg.exec(_.get(item,optionsSearch, ''))
    })
    let arrDaiLySearch = [ ...listDaiLySearch]
    const elmToRender = renderElement.list_phieu_xh(arrDaiLySearch);
    document.getElementById("body_list_item_phieuxh").innerHTML = elmToRender.join("");
}
const formatnew = () => {
    document.querySelector(".list_button").querySelector("button").style.display ="table-row"
    const elmArrInput = document.querySelector("#arr_input");
    
    const arrInput = elmArrInput.querySelectorAll("input");
    for(let index = 3 ; index < arrInput.length ; index++){
        arrInput.value=""
    }

}
const selectPhieuXH = (id_phieuxuat) => {
    const wrapArrInput = document.getElementById("arr_input");
    const arrInputKhuyenMai =  wrapArrInput.querySelectorAll("input");
    const arrSlect = wrapArrInput.querySelectorAll("select");
    const findmaKM = ListPhieuXHMain.find(item => item.id_phieuxuat === id_phieuxuat);
    arrInputKhuyenMai.forEach((itemKM) => {
        const itemInputKM = itemKM;
        const type = itemKM.dataset.type;
        
        itemInputKM.value = _.get(findmaKM,type,'');
      });

      document.querySelector(".list_button").querySelector("button").style.display ="none"

}
const chitietPXH= (id_phieuxuat) => {
    window.location.href = urlmain + "CT_PhieuXuat.html?id_phieuxuat="+id_phieuxuat;
}
const handleThemPhieuXH = async (elmProps) => {
    const dataAPI = await CallAPI.ADEPhieuXH(elmProps,'ADD');
    ListPhieuXHMain = [{...elmProps}, ...ListPhieuXHMain]
    renderListPhieuXH();
    formatnew()
}
const handleXoaPhieuXH = async (elmProps) => {
    const dataAPI = await CallAPI.ADEPhieuXH(elmProps,'DELETE');
    renderListPhieuXH();
    formatnew()

}
const handleSuaPhieuXH = async (elmProps) => {
    const dataAPI = await CallAPI.ADEPhieuXH(elmProps,'EDIT');
    renderListPhieuXH();
    formatnew()
}
const changeIDDaily = async (elm)=> {
    const dataThuTien = await CallAPI.getListPhieuThuTien();
        const{list_phieuthutien} = dataThuTien.getListPhieuThuTien;

    const value = elm.value;
    const valueDaiLy = list_phieuthutien.find(item => item.id_phieuthu === value);
    const elmArrInput = document.querySelector("#arr_input");
    console.log(valueDaiLy);
    elmArrInput.querySelectorAll("input").forEach(item => {
            
        item.value = _.get(valueDaiLy,item.dataset.type,"");
    })
    document.getElementById("ngaylapphieu").value = today;
}
const handleActionPhieuXH = async (elm) => {
    const elmArrInput = document.querySelector("#arr_input");


    if (window.getComputedStyle(elmArrInput, null).display === "none") {
        const dataThuTien = await CallAPI.getListPhieuThuTien();
        const{list_phieuthutien} = dataThuTien.getListPhieuThuTien;
      
        const arrIDHoaDon = [];
        if(list_phieuthutien.length < 1) return;
        list_phieuthutien.forEach(item => arrIDHoaDon.push({id_hoadon: item.id_hoadon, id_daily: item.id_daily, id_phieuthu: item.id_phieuthu}));
    
        const htmlHoaDon = arrIDHoaDon.map(item => `<option value="${item.id_phieuthu}">${item.id_phieuthu}</option>`)
        console.log(arrIDHoaDon[0])
        document.getElementById("select_phieuthu").innerHTML = htmlHoaDon.join();
        console.log(list_phieuthutien);
        elmArrInput.querySelectorAll("input").forEach(item => {
            
            item.value = _.get(list_phieuthutien[0],item.dataset.type,"");
        })
        document.getElementById("ngaylapphieu").value = today;

        return elmArrInput.style.display = "table-row"

    }
    else {
        elmArrInput.style.display = "none"
        const arrInput = elmArrInput.querySelectorAll("input");
        const formatObject = {};
        for(let index = 0 ; index < arrInput.length ; index++){
            const type = arrInput[index].dataset.type;
            const value = arrInput[index].value;

            _.set(formatObject, [type], value);
            if(type !=="ngaylapphieu") arrInput[index].value = ""
        }
        const phieuthu = document.getElementById("select_phieuthu").value;
        _.set(formatObject, ["id_phieuthu"],phieuthu);
        
        switch (elm.dataset.type) {
            case "ADD":
                return handleThemPhieuXH(formatObject)
            case "DELETE":
                return handleXoaPhieuXH(formatObject)
            case "EDIT":
                return handleSuaPhieuXH(formatObject)
        }
    }
}


const renderCTPXH =async () => {

    let id_phieuxuat =getParameterByName("id_phieuxuat")
    const queryInFor = document.querySelector(".input-infor-hoadon");
    const { getItemPhieuXuatHang } = await CallAPI.getItemPhieuXuatHang(id_phieuxuat);

    const {list_hoadon} =getItemPhieuXuatHang;


    const { list_phieu_xh } = getItemPhieuXuatHang;

    const { list_product } = list_hoadon[0];
    const hoadon = list_hoadon[0];
    const { tienthu, conno} = list_phieu_xh[0];


    let htmlSP = "";
    document.getElementById("body_list_ctpxh").innerHTML = htmlSP;


    let total_money =total_amount = 0;
      total_money = _.get(hoadon,"sum.total_price",0)
      total_amount = _.get(hoadon,"sum.amount",0)
      list_product.forEach((item, index) => {
        htmlSP += `
        <tr id="input-sp">
                <th width="100px">${index} </th>
                <th width="100px">${item.name}</th>
                <th width="100px">${item.dvtinh}</th>
                <th width="100px">${item.dongia}</th>
                <th width="100px">${item.soluong}</th>
                <th width="100px">${item.total_price}</th>
          </tr>
    `;
      });
    document.getElementById("body_list_ctpxh").innerHTML = htmlSP;
    document.getElementById("total_money").innerHTML = total_money;
    document.getElementById("total_money_2").innerHTML = total_money;
    document.getElementById("total_amount").innerHTML = total_amount;
    const MaKM = _.get(list_phieu_xh[0],'maKM',0)
    document.getElementById("khuyenmai").innerHTML ="Khuyến Mãi: " +  (((!MaKM ? 0 : MaKM)*parseInt(total_money)) / 100);
    document.getElementById("sotientra").innerHTML ="Số Tiền trả: " +  tienthu;
    document.getElementById("conlai").innerHTML ="Số Tiền còn lại: " + conno;
 
    const arrTitleCTPXH = document.querySelectorAll("#input-phieuthutien");

    arrTitleCTPXH.forEach(item => {
        item.value = _.get(list_phieu_xh[0],item.dataset.type,'');
    })


//     conno: "390"
// id_daily: "LcI4Jl"
// id_hoadon: "XJ8YUU"
// id_phieuxuat: "IffeEb"
// maKM: "BMTdzHidcwtlzWT"
// ngaylapphieu: "06/12/2020"
// sotientra: "100"
}